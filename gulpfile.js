var gulp = require("gulp"); // Подключаем Gulp
var concat = require("gulp-concat");
var autoprefixer = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var del = require("del");
var browserSync = require("browser-sync").create();
var reload = browserSync.reload;

const cssFiles = ["./app/css/*.css"];

//==========================

function styles() {
  return gulp
    .src(cssFiles)
    .pipe(concat("style.css"))

    .pipe(
      autoprefixer({
        cascade: false,
      })
    )

    .pipe(
      cleanCSS({
        level: 2,
      })
    )
    .pipe(gulp.dest("./dist/css/"))
    .pipe(browserSync.stream());
}

//==========================

function clean() {
  return del(["dist/*"]);
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch("./app/css/**/*.css", styles);
  gulp.watch("app/*.html").on("change", browserSync.reload);
}

gulp.task("styles", styles);

gulp.task("del", clean);

gulp.task("watch", watch);

gulp.task("build", gulp.series(clean, styles));

gulp.task("dev", gulp.series("build", "watch"));
